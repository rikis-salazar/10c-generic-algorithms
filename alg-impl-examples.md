# Generic algorithms (implementations)

The following equivalent implementations of some generic algorithms have been
taken from [`cppreference.com`][cpp-ref]. They are provided to illustrate the
use of some of the concepts we have discussed during lecture.

[cpp-ref]: en.cppreference.com

Note(s):

* Keep in mind that these are equivalent versions. There is no guarantee that
  this is the code that you'll find if you dig into your system libraries.

## `std::find`

Here we can watch _input iterators_ in action.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename InputIt, typename T>
InputIt find(InputIt first, InputIt last, const T& value) {
    for (; first != last; ++first) {
        if ( *first == value ) {
            return first;
        }
    }
    return last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## `std::find_if`

In addition to input iterators, here a _unary predicate_ is used to set policy.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename InputIt, typename UnaryPredicate>
InputIt find_if(InputIt first, InputIt last, UnaryPredicate p) {
    for (; first != last; ++first) {
        if ( p(*first) ) {
            return first;
        }
    }
    return last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

### `std::accumulate`

This is a single pass algorithm that does not require saving, hence input
iterators work just fine. The more generic version of this function allows the
programmer to use a custom _binary operation_ to "add" the elements in the
passed range.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename InputIt, typename T, 
          typename BinaryOperation = std::plus<T> >  // see note below
T accumulate(InputIt first, InputIt last, T init, BinaryOperation op) {
    for (; first != last; ++first) {
        init = op(init, *first);
    }
    return init;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Note(s):

* `std::plus<T>` is not part of the `C++11` standard. Instead it is part of the
  `C++14` standard. The same is true for `std::less<T>` below.


## `std::min_element`

Here an old concept reappears: _comparator_. Also, since the current minimum
needs to be saved, an input iterator is not enough. Hence the use of a _forward
iterator_.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename ForwardIt, class Compare = std::less<T> >  // see note above
ForwardIt min_element(ForwardIt first, ForwardIt last, Compare comp) {
    if (first == last) return last;
 
    ForwardIt smallest = first;
    ++first;
    for (; first != last; ++first) {
        if ( comp(*first, *smallest) ) {
            smallest = first;
        }
    }
    return smallest;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

### `std::copy`

This algorithm needs both, input iterators to read data, as well as _output
iterators_ to write data.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename InputIt, typename OutputIt>
OutputIt copy(InputIt first, InputIt last, OutputIt d_first) {
    while (first != last) {
        *d_first++ = *first++;
    }
    return d_first;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

### `std::copy_backwards`

According to [cppreference.com][cpp-ref], it  

> Copies the elements from the range, defined by `[first, last)`, to another
> range ending at `d_last`. The elements are copied in reverse order (the last
> element is copied first), but their relative order is preserved.

Because the decrement operator is expected to be available, _bidirectional
iterators_ are needed here.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename BidirIt1, typename BidirIt2>
BidirIt2 copy_backward(BidirIt1 first, BidirIt1 last, BidirIt2 d_last) {
    while (first != last) {
        *(--d_last) = *(--last);
    }
    return d_last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## `std::random_shuffle` (deprecated)

This example is included here just to illustrate a possible use of _random
access iterators_. However, be aware that this piece of code is not ideal
and the algorithm itself is slated for removal in the `C++17` standard.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename RandomIt >
void random_shuffle( RandomIt first, RandomIt last ) {
    typename std::iterator_traits<RandomIt>::difference_type i, n;
    n = last - first;
    for (i = n-1; i > 0; --i) {
        using std::swap;
        swap(first[i], first[std::rand() % (i+1)]);
        // Note:
        //     rand() % (i+1) isn't actually correct, because the generated
        //     number is not uniformly distributed for most values of i.
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice the difference of two iterators, as well as the use of `operator[]`. 
