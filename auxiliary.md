% Lecture: Generic Algorithms 
% Ricardo Salazar 
% May 25, 2018

# Generic algorithms

## About equivalent implementations

The following implementations of some generic algorithms have been taken from
[`cppreference.com`][cpp-ref]. They are provided to illustrate the use of some
of the concepts we have discussed during lecture.

[cpp-ref]: en.cppreference.com


Note(s):

*   There is no guarantee that this is the code you will find if you dig into
    your system libraries.


## `std::find`

What is the type of _iterators_ needed here?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename IteratorType, typename ItemType >
IteratorType find( IteratorType first, IteratorType last,
                   const T& value) {

    for ( ; first != last ; ++first )
        if ( *first == value ) 
            return first;

    return last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## `std::find_if`

In addition to iterators, here a functor-like object is used.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename IteratorType, typename FunctionObject >
IteratorType find_if( IteratorType first, IteratorType last,
                      FunctionObject f) {

    for ( ; first != last ; ++first )
        if ( f(*first) )
            return first;

    return last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## `std::accumulate`

This is a single pass algorithm (does not require saving). The more generic
version allows the use of a custom _binary operation_. If none is specified, the
default is "addition".

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename IteratorType, typename ItemType, 
          typename FunctionObject = std::plus<ItemType> >
ItemType accumulate( IteratorType first, IteratorType last,
                     ItemType init, FunctionObject op) {
    for ( ; first != last ; ++first )
        init = op(init, *first);

    return init;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Note(s):

* `std::plus<ItemType>` is part of the `C++14` standard.


## `std::min_element`

What is the type of the iterator? _Hint:_ `smallest` is reused.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename Iterator,
          typename FunctionObject = std::less<ItemType> >
Iterator min_element( Iterator first, Iterator last,
                      FunctionObject comp ) {
    if ( first == last )
        return last;

    Iterator smallest = first++;
    for ( ; first != last ; ++first )
        if ( comp(*first, *smallest) ) 
            smallest = first;

    return smallest;
}   // Note: std::less<ItemType> is also part of C++14.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## `std::copy`

This algorithm needs different types of iterators. Why?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename IteratorType1, typename IteratorType2 >
IteratorType2 copy( IteratorType1 first, IteratorType1 last,
                    IteratorType2 d_first) {

    while ( first != last ) 
        *d_first++ = *first++;

    return d_first;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## `std::copy_backwards`

Copies the elements from the range, defined by `[first, last)`, to another range
ending at `d_last`. The elements are copied in reverse order[^two], but their
relative order is preserved.  

What type of iterators are used? _Hint:_ requires `operator--`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename Iterator1, typename Iterator2 >
Iterator2 copy_backward( Iterator1 first, Iterator1 last,
                         Iterator2 d_last ) {

    while (first != last)
        *(--d_last) = *(--last);

    return d_last;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

[^two]: The last element is copied first.

## `std::random_shuffle` (deprecated)

Included to illustrate a possible use of _random access iterators_. However, the
code is not ideal, and the algorithm itself is slated for removal in the `C++17`
standard.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template< typename Iter >
void random_shuffle( Iter first, Iter last ) {
    typename std::iterator_traits<Iter>::difference_type i, n;
    n = last - first;
    for( i = n-1 ; i > 0 ; --i )
        std::swap(first[i], first[std::rand() % (i+1)]);
    // Note: rand() % (i+1) isn't actually correct, because
    // the numbers generated are not uniformly distributed.
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The use of the difference of iterators, as well as `operator[]`, make this code
not suitable for non random access iterators.
